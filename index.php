<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Canary Admin Panel</title>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
		
		<link rel="stylesheet" href="assets/css/style.css" >
		
    </head>
<body>
    <div class="container h-100">
		<div class="d-flex justify-content-center h-100">
			<div class="user_card">
				<div class="d-flex justify-content-center">
					<div class="brand_logo_container">
						<img src="assets/images/logo.png" class="brand_logo" alt="Logo">
					</div>
				</div>
				
				<div class="d-flex justify-content-center form_container">
					<form class="disenchantment" id=login-form>
						<div class="input-group mb-3">
							<div class="input-group-append">
								<span class="input-group-text"><i class="fas fa-user"></i></span>
							</div>
							<input id="email" type="email" name="email" class="form-control input_user" value="" placeholder="email">
						</div>
						<div class="input-group mb-2">
							<div class="input-group-append">
								<span class="input-group-text"><i class="fas fa-key"></i></span>
							</div>
							<input id="password" type="password" name="password" class="form-control input_pass" value="" placeholder="password">
						</div>
						<div class="form-group">
							<div class="custom-control custom-checkbox">
								<!--<input type="checkbox" class="custom-control-input" id="customControlInline">-->
								<p class="custom-control-label" id="error-msg"></p>
							</div>
						</div>
					</form>
				</div>
				<div class="d-flex justify-content-center mt-3" id="login_status"></div>
				<div class="d-flex justify-content-center mt-3 login_container">
					<button id="loginbutton" type="button" name="button" class="btn login_btn">Login</button>
				</div>
				<div class="mt-4">
					<div class="d-flex justify-content-center links">
						<a href="#" style="color:rgb(43, 41, 41)">Forgot your password?</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
<script src="https://code.jquery.com/jquery-3.2.1.js" integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
<script>
	$(function()
	{
		$('#loginbutton').click(function(e){

			let self = $(this);

			e.preventDefault(); // prevent default submit behavior

			self.prop('disabled',true); 

			var data = $('#login-form').serialize(); // get form data
			
			 // sending ajax request to login.php file, it will process login request and give response.
			//console.log(data);
			$.ajax({
				url: '/login.php',
				type: "POST",
				data: data,
			}).done(function(res) {
				res = JSON.parse(res);
				
				if (res['status']) // if login successful redirect user to secure.php page.
				{
					location.href = "secure.php"; // redirect user to secure.php location/page.
				} else {

					var errorMessage = '';
					// if there is any errors convert array of errors into html string, 
					//here we are wrapping errors into a paragraph tag.
					console.log(res.msg);
					$.each(res['msg'],function(index,message) {
						errorMessage += '<p>' + message+ '</p>';
					});
					// place the errors inside the div#error-msg.
					$("#error-msg").html(errorMessage);
					$("#error-msg").show(); // show it on the browser, default state, hide
					// remove disable attribute to the login button, 
					//to prevent multiple form submissions 
					//we have added this attribution on login from submit
					self.prop('disabled',false); 
				}
				
			}).fail(function() {
				alert("error");
			}).always(function(){
				self.prop('disabled',false); 
			});
		});
	});
</script>

</html>